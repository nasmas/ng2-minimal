### Replacement for Angular 2 quickstart ###

* Only requires the minimal packages for Angular 2
* Much faster initial npm install

### Requirements ###

* npm install -g typescript
* npm install -g lite-server

### Usage ###
* git clone <repo>
* npm install
* npm run tsc
* npm start

After this, developer must manually compile, via npm run tsc, after making changes.

To constantly recompile, which I find annoying since it triggers when emacs creates ~ files and makes me
try to save all files at once, do

* npm run tsc-watch

instead.  Developer will have to background this to do npm start in the same command window.