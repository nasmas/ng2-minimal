import { Component } from '@angular/core';

@Component({
    selector: 'my-app',
    template: `
        <h1>Hello, World from AppComponent</h1>
    `
})
export class AppComponent { }
